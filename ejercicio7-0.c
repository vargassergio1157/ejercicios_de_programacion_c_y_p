#include <stdio.h>
//programa que imprime una suma de numeros enteros y te da la opcion de decidir si seguir o terminar la operacion con el bucle do/while
int main(int argc, char const *argv[]) {
  char seguir;
  int acumulador,numero;
  acumulador=0;
  do {
    printf("\n introduzca un numero entero:" );
    scanf("%d",&numero);
    acumulador+=numero;

    printf("\n desea introducir otro numero(n/s):");
  fflush(stdin);
    scanf("%c",&seguir);
    printf("\n");
}while (seguir!='n');
  printf("la suma de los numeros es %d\n",acumulador);
  return 0;
}
