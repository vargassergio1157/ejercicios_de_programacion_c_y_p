#include <stdio.h>
//programa que imprime del 1 al 456 ,con un ciclo while controlado por contador
int main(int argc, char const *argv[]) {
  int contador;
  contador=1;
  while (contador<=456) {
    printf("%d\n",contador);
    contador++;
  }
  return 0;
}
