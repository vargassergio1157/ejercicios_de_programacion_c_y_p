#include <stdio.h>
#include <math.h>
//programa que imprime las raices cuadradas de los primeros numeros naturales
int main(int argc, char const *argv[]) {
  int a=1,b=2;
  for (int i = 1; i<=10; i++) {
    printf("%d - %.f\n",i,pow(a,b));
    a++;
  }
  return 0;
}
