#include <stdio.h>
#include <math.h>
#define PI 3.141592
//programa que muestra el area de un circulo al obtener en teclado el radio,si este es menor a 0 muestra,error,se le da opcion para salir del bucle con 0
int main(int argc, char const *argv[]) {
float radio,area;
do {
  printf("introduzca el radio(0 para terminar):");
  scanf("%f",&radio );
  if (radio<=-1) {
    printf("incorrecto,no hay areas positivas");
  }
  printf("el area total de la circunferencia es : %.2f",area=PI*pow(radio,2));
} while(radio!=0);

  return 0;
}
