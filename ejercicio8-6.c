#include <stdio.h>
//programa que imprime numeros pares del 1 al 30
int main(int argc, char const *argv[]) {

  for (int i = 2; i<=30; i+=2) {
    printf("%d\n",i);
  }
  return 0;
}
