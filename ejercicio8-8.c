#include <stdio.h>
//ejercicio que imprime las tablas de multiplicar del 3 al 10 con ciclo for
int main(int argc, char const *argv[]) {
  int a;
  for (int a = 3; a <= 10; a++) {
   for (int i =1; i <=10; i++) {
     printf("%d*%d=%d\n",a,i,a*i );
   }
   printf("******************\n");
  }
  return 0;
}
