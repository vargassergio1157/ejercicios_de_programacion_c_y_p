#include <stdio.h>
#include <math.h>
//programa que imprime las primeras 10 potencias de 2
int main(int argc, char const *argv[]) {

  for (int i=1; i<=10; i++) {
  printf("%.f",pow(2,i));
  printf("\n" );
  }
  return 0;
}/*se utiliza-lm al final del comando,tipo"gcc - o arg programa.c -lm",para declarar la
 funcion pow(i,x), especificamente para direccionar el codigo a la libreria matematica de gcc*/
