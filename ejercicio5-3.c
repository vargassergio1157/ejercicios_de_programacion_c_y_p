#include <stdio.h>
//programa que muestra en la pantalla si el numero capturado por el teclado es multiplo de 3 o no
int main(int argc, char const *argv[]) {
  int numero;
  printf("introduzca un numero:" );
  scanf("%d",&numero );
  if (numero%3==0) {
    printf(" el numero %d es multiplo de 3",numero );
  } else {
    printf("el numero %d no es multiplo de 3",numero );
  }
  return 0;
}
