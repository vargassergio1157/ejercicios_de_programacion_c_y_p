#include <stdio.h>
//programa que pide iintroducir un valor entero,posteriormente arroja su raiz cuadrada y da opcion para seguir o terminar
int main(int argc, char const *argv[]) {
  int numero,resultado,contador;
  char opcion;

    printf("introduzca un numero:\n");
    scanf("%d",&numero);
    printf("%d elevado al 2 es %d\n",numero,resultado=numero*numero);
    printf("desea continuar(s/n)\n");

    scanf("%*c%c",&opcion);/*cuidado con los caracteres en consola,en este caso,scanf lee automaticamente al momento de dar enter(despues de una instruccion printf/scanf)
                            el scanf el cual lee un salto de linea,ya que este se trata de un caracter,entonces al momento de la ejecucion tiende a arrojar habitualmente errores con printf
                            y scanf anidados y del tipo char*/
                            /*para la correccion de este error,se usa la funcion fflush para limpiar el buffer del teclado,pero eso solo funciona con windows por lo que parece
                            para corregir el error en liinux se utiliza un caracter del tipo conversion con un astedisco (%*c) para comerse el salto de linea y,despues se utiliza un getchar
                            para poder ingresar el dato correctamente*/
while(opcion!='n'){
  printf("introduzca un numero:\n");
  scanf("%d",&numero);
  printf("%d elevado al 2 es %d\n",numero,resultado=numero*numero);
  printf("desea continuar(s/n)\n");
  scanf("%*c%c",&opcion);
  getchar();
  contador++;
}

  return 0;
}
